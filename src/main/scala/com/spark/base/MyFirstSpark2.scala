package com.spark.base
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.SparkContext._

object MyFirstSpark2 {
  def main(args: Array[String]) {
    if (args.length != 2) {
      System.err.println("Usage:MyFirstSpark <Input> <Output>")
    }
    val conf = new SparkConf().setAppName("myFirstSpark")
    val sc = new SparkContext(conf)
    sc.textFile(args(0)).map(_.split("\t")).filter(_.length == 6).map(x => (x(1), 1)).reduceByKey(_ + _).map(x => (x._2, x._1))
      .sortByKey(false).map(x => (x._2, x._1)).saveAsTextFile(args(1))
    sc.stop()
  }
}
